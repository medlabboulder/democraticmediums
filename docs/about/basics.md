# Basics

The future of self-governance is being made in social movements, networked protocols, and unsettled governments around the world. What tools will they have to work with?

**This is a collaborative directory of democratic mediums—patterns for decision, deliberation, and noise.** It brings lessons from political, corporate, and community governance into conversation with the people inventing the next generation of institutions with permissionless networks and radical markets.

The directory collects discrete governance techniques that can be adapted, combined, and modified for new institutional designs. It draws on diverse research and experience to help such communities make informed choices about how they self-govern.

Democratic Mediums is a project of the [Media Enterprise Design Lab](https://colorado.edu/lab/medlab) at the University of Colorado Boulder, led by [Nathan Schneider](https://www.colorado.edu/cmci/people/media-studies/nathan-schneider). See the [Activity page on GitLab](https://gitlab.com/medlabboulder/democraticmediums/activity) for contributors.
