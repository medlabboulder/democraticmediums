# Start here

The future of self-governance is being made today. What tools do we have to work with?

<div id="pullquote">Democratic Mediums is a directory of patterns for decision, deliberation, and noise.</div>

This is an early-stage project that is mostly incomplete. Browse some sample entries: [Board](mediums/board.md), [Bureaucracy](mediums/bureaucracy.md), [Canvassing](mediums/canvassing.md), [Condorcet](mediums/condorcet.md), [Continuous voting](mediums/continuous_voting.md), [Delegation](mediums/delegation.md), [Do-ocracy](mediums/do-ocracy.md), [Federation](mediums/federation.md), [Ownership](mediums/ownership.md), [Quadratic voting](mediums/quadratic_voting.md), [Ritual](mediums/ritual.md), [Sortition](mediums/sortition.md).
