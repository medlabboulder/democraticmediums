# Ownership

Ownership is an umbrella term for a varying series of legal rights over particular entities. In the case of private corporate entities, ownership is a mechanism for claiming and clarifying governance rights.

**Input:** Corporate entity, specified rights of ownership

**Output:** Specified owner involvement in corporate governance

## Background

Modern corporate ownership emerged during the rise of colonial and industrial economies, as a means of managing the complex financial arrangements necessary to capitalize those endeavors.

Lawyers frequently refer to ownership as a "bundle of rights," suggesting that it is not a fixed or stable phenomenon but one that varies based on design and context.

## Feedback loops

### Sensitivities

* Clarifies in precise legal terms the governance claims of owners
* Typically organizes influence according to stake in the entity to prevent free-riding

### Oversights

* Does not recognize the voices of important non-owning stakeholders
* May obscure important externalities, such as social and environmental considerations
* Inapplicable to non-ownable entities, like nonprofit organizations, information commons, and public-sector resources

## Implementations

### Communities

* The [cooperative business tradition](https://www.ica.coop/en/cooperatives/what-is-a-cooperative) tends to employ ownership rights on a one-person-one-vote basis, rather than the one-share-one-vote model of most other corporate practice

### Tools

* [Encode](https://encode.org/) offers "integrations of proven models, legal rules, and methodologies for fully distributing authority and ownership in a purpose-driven business"
* [Upstock](https://upstock.io/) "creates tools and shares knowledge that aligns founders, investors, and team members" through equity compensation

## Further resources

* Chassagnon, V., & Hollandts, X. (2014). "[Who Are the Owners of the Firm: Shareholders, Employees, or No One?](https://doi.org/10.1017/S1744137413000301)" _Journal of Institutional Economics_, 10 (1), 47–69.
* Hansmann, H. (1996). _The Ownership of Enterprise_.  Cambridge, MA: Harvard University Press.
* Kelly, M. (2012). _Owning Our Future: The Emerging Ownership Revolution_. San Francisco: Berrett-Koehler Publishers.