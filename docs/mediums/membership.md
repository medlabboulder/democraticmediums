# Membership

Membership is a means by which an organization or government determines its core constituency---and excludes non-members. Members enjoy some specific set of rights of governance and benefit that non-members do not enjoy. In state contexts, it is generally referred to as citizenship. In corporate contexts, it may correlate with stock [ownership](ownership.md). Systems of membership typically presume some means of identity management.

**Input:** identity management, set of membership rights and benefits

**Output:** enjoyment of rights and benefits by members

## Background

<!-- brief historical reference points -->

## Feedback loops

### Sensitivities

<!-- what does this medium capture especially well? -->

### Oversights

<!-- what does this medium overlook? -->

## Implementations

### Communities

<!-- what are some real-world examples in practice? -->

### Tools

<!-- what software, methodologies, or organizations are available to facilitate implementation -->

## Further resources

<!-- where should people look first if they want to learn more? -->
