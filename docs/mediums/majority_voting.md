# Majority voting

Majority voting is an electoral system requiring a candidate to receive more than half of the votes cast to win and results in one single winner. Though associated with plurality voting, as they are both proportional representative systems, majority voting does not select a winner based on the most votes but on the candidate who receives more than fifty percent of the votes. If no one candidate receives half of the votes, either a runoff election is held between the two candidates with the most votes or an alternative vote or “majority preferential” system is used.

**Input:** a position up for election; eligible voters; a specific candidate that receives at least half of the votes cast

**Output:** a single victor who represents the will of the most voters

## Background

Majority voting has roots in the democracy of Ancient Greece when decisions were made by a popular assembly of eligible male voters. Limits existed on majority voting practices, however, to ensure the fair distribution of power among classes and interests through simultaneously giving proportionate representation to minority groups.

The ideology of majority voting systems was later adopted by thinkers such as John Locke and Jean-Jacques Rousseau, for whom it represented equality and fairness due to equal valuation of individual votes. 


## Feedback loops

### Sensitivities

* A majority rule is said to embody democratic ideals by representing the will of the most people
* This system can lead to more middle-ground solutions, as it often represents the will of the median voter
* It likely produces a stable government
* It is apt to protect against oppressive governments

### Oversights

* These systems can fail to give minority groups any voice or proportional platform; it has been criticized as [“tyranny of the majority”]( https://www.annenbergclassroom.org/glossary_term/majority-rule-and-minority-rights/)
* Minority parties have little chance at winning an election
* This system can be manipulated by gerrymandering and border adjustment
* Voters may choose a candidate who does not fully represent their beliefs, but whom they believe may have a better chance of winning as to not “waste” their vote

## Implementations

### Communities

Democracies across the world utilize majority voting in some way, including Finland, Austria, France and Portugal. In the United States, a presidential candidate must have a majority of Electoral College votes to be elected. 

This system is implemented in [many corporate governance systems]( https://www.covfinancialservices.com/2016/11/2017-proxy-season-preview-renewed-shareholder-push-for-majority-voting-in-director-elections-may-affect-more-small-and-middle-market-banks/), such as the [Council of Institutional Investors’ directorial elections]( https://www.cii.org/majority_voting_directors).

### Tools

* Algorithms such as the [Boyer—Moore majority vote algorithm]( https://algorithms.tutorialhorizon.com/majority-element-boyer-moore-majority-vote-algorithm/) or the [MJRTY majority vote algorithm]( https://medium.com/stephen-rambles/an-efficient-majority-vote-algorithm-3005722180d4) help determine if a majority exists in a list of votes.
* A variety of simple poll tools exist online to facilitate majority voting. Social media platforms such as Facebook, Twitter, and Instagram offer poll features while other websites such as Doodle Poll, EasyPolls, and Google offer tools for voting.

## Further resources

* Volk, K. G. (2014). Moral minorities and the making of American democracy. Oxford University Press.
* Tullock, G. (1959). Problems of majority voting. Journal of political economy, 67(6), 571-579.
* Rubinstein, A. (1980). Stability of decision systems under majority rule. Journal of Economic Theory, 23(2), 150-159.
* Boyer, R. S., & Moore, J. S. (1991). MJRTY—a fast majority vote algorithm. In Automated Reasoning (pp. 105-117). Springer, Dordrecht.
