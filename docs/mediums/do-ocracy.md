# Do-ocracy

Do-ocracy is a methodology by which those who take initative to do work in a group are empowered to make decisions about what they do. It tends to appear most often in informal organizations characterized by high levels of mutual trust and a dependence on contributions by highly motivated volunteers.

**Input:** self-directed individuals or subgroups, tacit permission from larger group

**Output:** self-directed outcomes

## Background

A portmanteau of the verb "to do" and the suffix "-ocracy," the word *do-ocracy* first appears [in Word Spy](https://wordspy.com/index.php?word=do-ocracy) in 2004. It appears again in 2012, in reference to a co-working space and the hacker phenomenon Anonymous. The radio program _A Way with Words_ [attributes](https://www.waywordradio.org/do-ocracy/) the popularization of the word with the 2011 Occupy movement.

[According to CommunityWiki](https://communitywiki.org/wiki/DoOcracy), "The term is popular with libertarian management afficionados and BurningMan participants." The wiki for the San Francisco hackerspace Noisebridge refers to it as "a decentralized, anarchist way of deciding and managing how things get changed" and offers this summary: "If you want something done, do it, but remember to be excellent to each other when doing so."

## Feedback loops

### Sensitivities

* Participant initative and sense of ownership
* Eliminates cost of formal governance practices in high-trust contexts
* Authority closest to the question at hand

### Oversights

* Unintended conflicts when participants act against the will of others
* Empowerment those with the most free time with the most authority
* Burnout when responsibilities aren't adequately shared
* Lack of coordination and coherence in group efforts

## Implementations

### Communities

* Edgeryders, a European social innovation network, [has as a motto](https://www.thenation.com/article/can-monasteries-be-model-reclaiming-tech-culture-good/), "Who does the work calls the shots"
* [Ouishare](https://www.ouishare.net/our-dna), a collaborative economy network, holds do-ocracy as one of its core "values"
    - Alícia Trepat Pont, "[A doocracy-based decision-making mechanism](https://communitiesforimpact.org/case_study/governance-to-empower-an-emergent-community/)," case study, Virtual Communities for Impact

### Tools

Do-ocracy practices can be facilitated by common collaboration tools like Git issue systems and wekan boards

## Further resources

* [DoOcracy entry at CommunityWiki](https://communitywiki.org/wiki/DoOcracy)
* [Lazy consensus](lazy_consensus.md) blends do-ocracy with [consensus process](consensus_process.md)
