# Lazy consensus

Lazy consensus is a method for decision-making according to which proposals within a group may be presumed to pass unless any explicit objections arise. It blends features of [do-ocracy](do-ocracy.md) and [consensus process](consensus_process.md). The Apache Software Foundation, which holds lazy consensus as a value, [summarizes](https://community.apache.org/committers/lazyConsensus.html) the method as "silence is consent."

**Input:** proposal within group, presumption of approval

**Output:** approval by silence or objection that triggers further deliberation

## Background

Lazy consensus is most widely employed among free/open-source software projects.

## Feedback loops

### Sensitivities

* Empowers participants to take initiative and rewards commitment with authority
* Lowers cost of governance

### Oversights

* Privileges voices of participants with more time and attention due to external inequalities
* Insuffient participation can lead to insufficient decision-making oversight
* Ambiguity arising from community silence (cf. "[Warnock's dilemma](https://en.wikipedia.org/wiki/Warnock%27s_dilemma)")

## Implementations

### Communities

* [Apache Software Foundation](https://community.apache.org/committers/lazyConsensus.html)

### Tools

Free/open-source projects that employ lazy consensus often rely on email discussion lists for decision-making. The method helps reduce the traffic on such lists.

## Further resources

* Nowviskie, Bethany. March 10, 2012. "[Lazy Consensus](http://nowviskie.org/2012/lazy-consensus/)." Based on #code4lib conference keynote.
