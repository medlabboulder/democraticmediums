# Transparency

Transparency in governance refers to the amount of information disclosed about a particular decision, process, or action; it serves as a way to hold accountable groups of people in power.

**Input:** available, honest, and open information on governance processes, decision-making, and allocation of resources for stakeholders and those governed

**Output:** more accountable governance system that offer readily accessible, in-depth information to relevant participants or inquirers 

## Background

An early Western example of transparency can be found in Athenian democracy and the Roman Republic; public finances were overseen by a board of citizens.

The Pendleton Act of 1883 was an early attempt to add transparency to the United States government by outlining regulations and pay classifications for civil servants. By the 1950s, most Western European countries developed rules to ensure transparency; such regulations are a requirement for countries in the European Union and the Council of Europe. In 1966, Lyndon Johnson passed the Freedom of Information Act in the United States and a host of other transparency regulations were passed throughout the 1970’s.

## Feedback loops

### Sensitivities

* Provides greater information for constituencies
* Informed constituencies hold those in positions of power accountable and spark productive dialogue
* Create an atmosphere that is forward-facing due to availability of information – officials don’t need to explain past decisions and processes to already-informed citizens
* Policies tend to be better for constituents due to aforementioned benefits
* Allocation of financial resources can be more intentional and effective in a transparent system
* Within an organization, transparency can lead to a more productive workflow because each members of the organization is informed and decision-making is clear and collaborative

### Oversights

* Implementing transparency mechanisms into preexisting governance system can have some start-up costs and create new responsibilities for employees
* Though information may be readily available, it may be misinterpreted, misrepresented, or used for nefarious purposes
* Organizations may face a difficult balance in public transparency and keeping important information confidential

## Implementations

### Communities

Government efforts at transparency vary; the city of Louisville, KY makes government spending transparent with their [“Your Tax Dollars at Work” tool](https://louisvilleky.gov/government/management-budget/services/your-tax-dollars-work) while other cities offer exhaustive, searchable sites with transparent information about projects and spending.

Other organizations and companies operate in a transparent manner, such as Patagonia – where you can track your clothing’s environmental footprint – and Buffer, a social media company whose transparency began with having revenue publically available on their website.

### Tools

* SumAll, hailed as a company succeeding greatly at transparency, recommends tools such as Slack and Wrike for communication and project management in [this](https://blog.sumall.com/journal/tools-we-use-for-transparency.html) blog post.
* Monster offers a [transparency assessment](https://hiring.monster.com/employer-resources/workforce-management/company-culture/corporate-transparency/) for corporate entities to better understand and improve their own practices. 
* Open Society Foundation offers [this]( https://web.archive.org/web/20170218030712/http://www.transparency-initiative.org/wp-content/uploads/2011/07/Opening-Government3.pdf) guide to best practices in transparency.

## Further resources

* Pozen, D. E., & Schudson, M. (2018). Troubling transparency: The history and future of freedom of information. New York: Columbia University Press.
* Bolivar, M. P. R., Galera, A. N., & Munoz, L. A. (2015). Governance, transparency and accountability: An international comparison. Journal of Policy Modeling, 37(1), 136-174. doi:10.1016/j.jpolmod.2015.01.010
* Kosack, S., & Fung, A. (2014). Does transparency improve governance? Annual Review of Political Science, 17(1), 65-87. doi:10.1146/annurev-polisci-032210-144356
