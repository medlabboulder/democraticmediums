# Judiciary

Judiciary refers to the system of courts a government system might include as an enactment of the separation of powers. As an interpreter of the law, the judiciary branch protects human rights and applies or disputes particular legislation to specific cases. Though it does not create or pass laws, it enacts them and may determine their scope or practical application.

**Input:** democratic government with separate branches of power; established court system operating at various levels

**Output:** opportunity for individuals to seek justice for wrongdoings; enforcement of laws; protection of constitutions

## Background

In ancient Rome, the judicial process was the first occurrence of legal proceedings in which the head judiciary member would review a case followed by a jury of sorts, comprised of Roman citizens who had little experience in law but were directed towards laws to apply and interpret for given cases. An Anglo-Saxon history notes the struggle between judges and monarchs from approximately 1000 A.D. – judges were initially previous advisors of the King – for independence. Bribery and corruption were rampant issues in these systems; the current judiciary system for this region was not developed until the 1960s.

In the United States, the judiciary branch began developing with the Judiciary Act of 1789, giving shape to Supreme Court regulations and federal district courts throughout the states.

## Feedback loops

### Sensitivities
* Provides a system of checks and balances for other branches of government; checks constitutionality of other branches’ decisions and laws
* Can uphold human rights through interpretation of law
* Allows individual attention and consideration to be paid to unique and complex cases
* If a court rules in a way that is perceived unjust, the decision can be appealed to a higher court for reconsideration
* Lifetime judiciary roles can prevent judges from acting on third-party or private interests for reelection purposes

### Oversights
* Selection of judges in some systems can contradict separation of powers, such as election through legislature or executive appointment
* Lifetime judiciary roles can lead to entrenchment and party-based rulings
* Ideological rulings and self-interest of judges can compromise neutral application of the law

## Implementations

### Communities

* Nearly all governments around the world include judiciaries of some form, such as China’s National People’s Congress, France’s Council of State and Court of Cassation, and Australia’s High Court.
* Blockchain-based distributed ledger systems [sometimes use mechanisms called oracles](https://cointelegraph.com/explained/blockchain-oracles-explained) that introduce human intervention to interpret context in order to decide whether or how to trigger a smart contract.

### Tools

* [Aragon Court](https://blog.aragon.org/aragon-court-is-live-on-mainnet/), "a dispute resolution protocol formed by jurors to handle subjective disputes that cannot be resolved by smart contracts"
* [Crowdjury]( https://medium.com/the-crowdjury/the-crowdjury-a-crowdsourced-court-system-for-the-collaboration-era-66da002750d8) is a hypothetical blockchain system for justice in the internet age.
* Software for court case management exists in a variety of places online, such as [eCourt](https://www.capterra.com/p/83916/eCourt/).
* [Voltaire](https://voltaireapp.com/) provides a hub for jury research.


## Further resources
* Berger, R. (1977). Government by judiciary (pp. 363-72). Cambridge, MA: Harvard University Press.
* Crowe, J. (2012). Building the judiciary : Law, courts, and the politics of institutional development. Retrieved from https://ebookcentral.proquest.com
* [Judiciary](https://link-gale-com.colorado.idm.oclc.org/apps/doc/CX1337702473/OVIC?u=coloboulder&sid=OVIC&xid=8a841947). (2011). In D. Batten (Ed.), Gale Encyclopedia of American Law (3rd ed., Vol. 6, pp. 74-79). Detroit, MI: Gale.
* Ruggiu, I. (2018). [Culture and the judiciary : The anthropologist judge](https://ebookcentral.proquest.com).

