# Polling

Polling is a democratic medium used to solicit public opinions or attitudes about a particular issue, usually for analysis. The level of formality of polling can range from a generalized questionnaire to accurate, in-depth elections that take place at a polling site. They can serve as indications or predictions of election results, as actual elections, as electoral verification, or as a way to simply gauge preferences and opinions on a variety of matters.

**Input:** vetting opinions of representative groups of people via written or verbal questions
 
**Output:** broad understanding of public opinion

## Background

[PBS](http://www.pbs.org/now/politics/polling.html) cites the first recognition of the importance of public opinion as occurring in the years before the French Revolution by the French Minister of Finance. In the United States, polling is associated with presidential election predictors beginning with the “straw polls” of 1824. Literary Digest was an organization founded in 1890 with a reputation for accurate polling until a 1936 debacle that preceded its merge with two similar publications. The Gallup organization was founded in 1935 and is still functioning today as a pinnacle of polling.

## Feedback loops

### Sensitivities

* Can offer an accurate and broad representation of public opinion at a relatively low cost
* Can offer a platform for marginalized voices to share their opinion and views
* Can provide valuable insights for politicians and corporations to better serve the needs of their populace
* Can hold people in positions of power accountable through understanding how individuals feel about actions, regulations, and laws

### Oversights

* Reducing the margin of error and therefore ensuring accuracy of a poll requires quite a large sample size
* Response bias may lead to false results by participants not giving their true opinion
* Nonresponse bias may lead to false results due to the particular characteristics of those who choose to participate in the poll
* Polling results covered in mainstream media can influence the opinions of voters based on bandwagon effect
* The format, wording, and accessibility of polls can influence the responses in manipulative ways

## Implementations

### Communities

As stated above, presidential elections in the United States are a prime site for public polling. Corporations use polls to gauge customer satisfaction and interest; advertisers use polls to see what products and ads work for particular demographics; politicians on any level can use polls to understand how they can better serve the needs of their constituents. 
[Gallup polls](https://news.gallup.com/poll/101905/gallup-poll.aspx) address economic issues, politics, and even health opinions. [Pew Research Center](https://www.pewresearch.org/topics/polling/) is known for a variety of polling information about a plethora of issues. Almost every major TV network conducts polls on relevant issues as well.

### Tools

* Online poll generators exist on a variety of platforms, from Doodle Poll to Survey Monkey to Google forms
* [American Research Group, Inc.](https://americanresearchgroup.com/moe.html) offers a margin of error calculator for those conducting polls
*The [American Association for Public Opinion Research](https://www.aapor.org/Education-Resources/Election-Polling-Resources/Sampling-Methods-for-Political-Polling.aspx) offers online resources for accurate polling

## Further resources

* Killesteyn, E. (2015). Polling officials: The strength and weakness of democratic systems. Election Law Journal, 14(4), 411-423. doi:10.1089/elj.2015.0308
* Moon, N. (1999). Opinion polls: History, theory and practice. Manchester: Manchester University Press.
* Smith, T. (1990). The First Straw?: A Study of the Origins of Election Polls. The Public Opinion Quarterly, 54(1), 21-36. 
